# 度假酒店管理系统

#### Description
度假酒店管理系统的主要任务，通过实现酒店房间类型和剩余的计算机管理，以提高工作效率。提供及时、广泛的信息服务，加快信息检索的效率，实况灵活的查询，减轻管理人员制作报表和统计分析的负担，且系统规模不太大但又要保证支持日常工作的要求，以便系统应易于扩充，方便日后统一联网与管理，提高管理水平。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
